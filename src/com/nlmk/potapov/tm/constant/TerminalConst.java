package com.nlmk.potapov.tm.constant;

public final class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";

}
